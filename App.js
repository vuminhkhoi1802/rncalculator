import React from 'react';
import {View} from 'react-native';
import {Calculator} from './components/Calculator';

const App = () => {
  return (
    <View
      style={{
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
      }}>
      <Calculator />
    </View>
  );
};

export default App;
