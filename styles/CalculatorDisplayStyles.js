import {StyleSheet} from 'react-native';

export const calculatorDisplayStyles = StyleSheet.create({
  root: {
    backgroundColor: '#1c191c',
    flex: 1,
    justifyContent: 'center',
  },
  text: {
    alignSelf: 'flex-end',
    color: 'white',
    fontSize: 72,
    paddingHorizontal: 30,
    right: 0,
    // transformOrigin: 'right',
  },
});
