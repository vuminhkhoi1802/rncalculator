import {StyleSheet, Dimensions} from 'react-native';

export const calculatorStyles = StyleSheet.create({
  root: {
    width: 320,
    height: 600,
    backgroundColor: 'black',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.8,
    shadowRadius: 2,
  },
  keypad: {
    height: 400,
    flexDirection: 'row',
  },
  inputKeys: {
    width: 240,
  },
  calculatorKeyText: {
    fontSize: 24,
  },
  functionKeys: {
    backgroundColor: 'rgba(202,202,204,1)',
    flexDirection: 'row',
    flexWrap: 'wrap-reverse',
    alignItems: 'center',
    justifyContent: 'center',
  },
  functionKeyText: {
    fontSize: 24,
    fontWeight: 'bold',
    color: 'black',
  },
  digitKeys: {
    backgroundColor: '#e0e0e7',
    flexDirection: 'row',
    flexWrap: 'wrap-reverse',
    alignItems: 'center',
    justifyContent: 'center',
  },
  digitKeyText: {
    fontSize: 27,
  },
  operatorKeys: {
    backgroundColor: '#94500c',
  },
  operatorKey: {
    borderRightWidth: 0,
  },
  operatorKeyText: {
    color: 'white',
    fontSize: 36,
  },
  keyMultiplyText: {
    lineHeight: 50,
  },
  keySign: {
    flex: 1,
  },
  keyPercent: {
    flex: 1,
  },
  key0: {
    paddingLeft: 32,
    width: 160,
  },
  keyDot: {
    overflow: 'hidden',
  },
  keyDotText: {
    fontSize: 60,
    marginTop: -10,
  },
});
