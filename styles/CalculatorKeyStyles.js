import {StyleSheet} from 'react-native';

export const calculatorKeyStyles = StyleSheet.create({
  root: {
    width: 80,
    height: 80,
    borderTopWidth: 1,
    borderTopColor: '#777',
    borderStyle: 'solid',
    borderRightWidth: 1,
    borderRightColor: '#666',
    // outline: 'none',
  },
  text: {
    lineHeight: 80,
    textAlign: 'center',
  },
});
