/**
 * Main Calculator class
 */

import React from 'react';
import {View} from 'react-native';
import {calculatorOperations} from './CalculatorOperations';
import {CalculatorKey} from './CalculatorKey';
import {calculatorStyles} from '../styles/CalculatorStyles';
import {KeyBoardInput} from './KeyBoardInput';
import {CalculatorDisplay} from './CalculatorDisplay';

const initialState = {
  value: null,
  displayValue: '0',
  operator: null,
  waitingForOperand: false,
};

export class Calculator extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = initialState;
  }

  clearAll() {
    this.setState(initialState);
  }

  clearDisplay() {
    this.setState({
      displayValue: '0',
    });
  }

  clearLastChar() {
    const {displayValue} = this.state;
    this.setState({
      displayValue: displayValue.substring(0, displayValue.length - 1) || '0',
    });
  }

  toggleSign() {
    const {displayValue} = this.state;
    const newValue = parseFloat(displayValue) * -1;
    this.setState({
      displayValue: String(newValue),
    });
  }

  inputPercent() {
    const {displayValue} = this.state;
    const currentValue = parseFloat(displayValue);

    if (currentValue === 0) {
      return null;
    }

    const fixedDigits = displayValue.replace(/^-?\d*\.?/, '');
    const newValue = parseFloat(displayValue) / 100;

    this.setState({
      displayValue: String(newValue.toFixed(fixedDigits.length + 2)),
    });
  }

  inputDot() {
    const {displayValue} = this.state;

    if (!/\./.test(displayValue)) {
      this.setState({
        displayValue: displayValue + '.',
        waitingForOperand: false,
      });
    }
  }

  inputDigit(digit) {
    const {displayValue, waitingForOperand} = this.state;

    if (waitingForOperand) {
      this.setState({
        displayValue: String(digit),
        waitingForOperand: false,
      });
    } else {
      this.setState({
        displayValue:
          displayValue === '0' ? String(digit) : displayValue + digit,
      });
    }
  }

  performOperation(nextOperator) {
    const {value, displayValue, operator} = this.state;
    const inputValue = parseFloat(displayValue);

    if (value == null) {
      this.setState({
        value: inputValue,
      });
    } else if (operator) {
      const currentValue = value || 0;
      const newValue = calculatorOperations[operator](currentValue, inputValue);

      this.setState({
        value: newValue,
        displayValue: String(newValue),
      });
    }

    this.setState({
      waitingForOperand: true,
      operator: nextOperator,
    });
  }

  handleKeyDown(event) {
    let {key} = event;

    if (key === 'Enter') {
      key = '=';
    }

    if (/\d/.test(key)) {
      event.preventDefault();
      this.inputDigit(parseInt(key, 10));
    } else if (key in calculatorOperations) {
      event.preventDefault();
      this.performOperation(key);
    } else if (key === '.') {
      event.preventDefault();
      this.inputDot();
    } else if (key === '%') {
      event.preventDefault();
      this.inputPercent();
    } else if (key === 'Backspace') {
      event.preventDefault();
      this.clearLastChar();
    } else if (key === 'Clear') {
      event.preventDefault();
      if (this.state.displayValue !== '0') {
        this.clearDisplay();
      } else {
        this.clearAll();
      }
    }
  }

  render() {
    const {displayValue} = this.state;
    const clearDisplay = displayValue !== '0';
    const clearText = clearDisplay ? 'C' : 'AC';

    return (
      <View style={calculatorStyles.root}>
        <KeyBoardInput onKeyDown={event => this.handleKeyDown(event)} />
        <CalculatorDisplay value={displayValue} />
        <View style={calculatorStyles.keypad}>
          <View style={calculatorStyles.inputKeys}>
            <View style={calculatorStyles.functionKeys}>
              <FunctionKey
                onPress={() =>
                  clearDisplay ? this.clearDisplay() : this.clearAll()
                }>
                {clearText}
              </FunctionKey>
              <FunctionKey
                onPress={() => this.toggleSign()}
                style={calculatorStyles.keySign}>
                ±
              </FunctionKey>
              <FunctionKey
                onPress={() => this.inputPercent()}
                style={calculatorStyles.keyPercent}>
                %
              </FunctionKey>
            </View>
            <View style={calculatorStyles.digitKeys}>
              <DigitKey
                onPress={() => this.inputDigit(0)}
                style={calculatorStyles.key0}
                textStyle={{textAlign: 'left'}}>
                0
              </DigitKey>
              <DigitKey
                onPress={() => this.inputDot()}
                style={calculatorStyles.keyDot}
                textStyle={calculatorStyles.keyDotText}>
                .
              </DigitKey>
              <DigitKey onPress={() => this.inputDigit(1)}>1</DigitKey>
              <DigitKey onPress={() => this.inputDigit(2)}>2</DigitKey>
              <DigitKey onPress={() => this.inputDigit(3)}>3</DigitKey>
              <DigitKey onPress={() => this.inputDigit(4)}>4</DigitKey>
              <DigitKey onPress={() => this.inputDigit(5)}>5</DigitKey>
              <DigitKey onPress={() => this.inputDigit(6)}>6</DigitKey>
              <DigitKey onPress={() => this.inputDigit(7)}>7</DigitKey>
              <DigitKey onPress={() => this.inputDigit(8)}>8</DigitKey>
              <DigitKey onPress={() => this.inputDigit(9)}>9</DigitKey>
            </View>
          </View>
          <View style={calculatorStyles.operatorKeys}>
            <OperatorKey onPress={() => this.performOperation('/')}>
              ÷
            </OperatorKey>
            <OperatorKey onPress={() => this.performOperation('*')}>
              ×
            </OperatorKey>
            <OperatorKey onPress={() => this.performOperation('-')}>
              −
            </OperatorKey>
            <OperatorKey onPress={() => this.performOperation('+')}>
              +
            </OperatorKey>
            <OperatorKey onPress={() => this.performOperation('=')}>
              =
            </OperatorKey>
          </View>
        </View>
      </View>
    );
  }
}

const DigitKey = props => (
  <CalculatorKey
    {...props}
    style={[calculatorStyles.digitKeys, props.style]}
    textStyle={[calculatorStyles.digitKeyText, props.textStyle]}
  />
);

const FunctionKey = props => (
  <CalculatorKey
    {...props}
    style={[calculatorStyles.functionKeys, props.style]}
    textStyle={[calculatorStyles.functionKeyText, props.textStyle]}
  />
);

const OperatorKey = props => (
  <CalculatorKey
    {...props}
    style={[calculatorStyles.operatorKey, props.style]}
    textStyle={[calculatorStyles.operatorKeyText, props.textStyle]}
  />
);
