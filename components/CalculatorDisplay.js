import React from 'react';
import {View, Text} from 'react-native';
import {calculatorDisplayStyles} from '../styles/CalculatorDisplayStyles';

export class CalculatorDisplay extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      scale: 1,
    };
  }

  handleTextLayout = e => {
    const {scale} = this.state;
    const {width, x} = e.nativeEvent.layout;
    const maxWidth = width + x;
    const actualWidth = width / scale;
    const newScale = maxWidth / actualWidth;
    if (x < 0) {
      this.setState({scale: newScale});
    } else if (x > width) {
      this.setState({scale: 1});
    }
  };

  render() {
    const {value} = this.props;
    const {scale} = this.state;

    const language = 'en-US';
    let formattedValue = parseFloat(value).toLocaleString(language, {
      useGrouping: true,
      maximumFractionDigits: 6,
    });

    const trailingZeros = value.match(/\.0*$/gm);

    if (trailingZeros) {
      formattedValue += trailingZeros;
    }

    return (
      <View style={calculatorDisplayStyles.root}>
        <Text
          children={formattedValue}
          onLayout={this.handleTextLayout}
          style={[calculatorDisplayStyles.text, {transform: [{scale}]}]}
        />
      </View>
    );
  }
}
