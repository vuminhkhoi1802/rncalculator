import React from 'react';
import {TouchableHighlight, Text} from 'react-native';
import {calculatorKeyStyles} from '../styles/CalculatorKeyStyles';

export class CalculatorKey extends React.Component {
  render() {
    const {children, onPress, style, textStyle} = this.props;
    return (
      <TouchableHighlight
        accessibilityRole="button"
        onPress={onPress}
        style={[calculatorKeyStyles.root, style]}
        underlayColor="rgba(0,0,0,0.25)">
        <Text
          children={children}
          style={[calculatorKeyStyles.text, textStyle]}
        />
      </TouchableHighlight>
    );
  }
}
