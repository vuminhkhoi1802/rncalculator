import React from 'react';

export class KeyBoardInput extends React.Component {
  handleKeyDown(event) {
    if (this.props.onKeyDown) {
      this.props.onKeyDown(event);
    }
  }

  componentDidMount() {
    this.handleKeyDown = this.handleKeyDown.bind(this);
  }

  render() {
    return null;
  }
}
