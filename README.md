React Native Calculator Application
=================

This is a simple Calculator Application created using [React-Native Framework](https://facebook.github.io/react-native/docs/getting-started)

Package Pre-Requisite
---------------
- node >= 10.15.x - [How to install Node](https://nodejs.org/en/download/) this will get you Node Package Manager (npm)
- yarn >= 1.16.x - [How to install Yarn](https://yarnpkg.com/en/docs/install)
- Android Studio - [How to install Android Studio](https://developer.android.com/studio/install)
- Android Emulator - [How to setup Android Emulator](https://docs.expo.io/versions/latest/workflow/android-studio-emulator/)
- React Native Command Line Interface - ```$ yarn add global react-native-cli``` or ```$ npm install -g react-native-cli```

Add to `~/.zshrc` or `~/.bashrc` file with the following lines
```
export ANDROID_HOME=$HOME/Android/Sdk
export ANDROID_SDK_ROOT=$HOME/Android/Sdk
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/platform-tools 
```

Run the following command to start the android emulator 

```
$ ${ANDROID_SDK_ROOT}/emulator/emulator -avd <emulatorName>
```
Getting Started
---------------
- Step 1: Install all the Pre-Requisite packages listed above

- Step 2: Clone this repository using the method of your choice `SSH` or `HTTPs`

- Step 3: Navigate to the project directory

- Step 4: Run the command below to install all the dependencies 
```bash
$ yarn install
```

- Step 5: Run the command below to start the Metro Bundler server
```bash
$ yarn start
```

- Step 6: On a separate Terminal Tab/Window, run the command below to start the application
```
$ react-native run-android
```

Screenshots
---------------
![Imgur](https://i.imgur.com/yv5VLce.png)
![Imgur](https://i.imgur.com/AoO8aQq.png)
